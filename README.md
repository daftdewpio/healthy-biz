1.  Run npm install
2.  Create .env file into repo and set the following attributes:
    email = {your email}
    clientid = {your secret id}
    clientsecretid = {your secret id}
    appid = {appid}
3.  Npm run start  
4.  Visit localhost:3000 in a web browser

This is a very rough and fragile PoC not designed for production, designed using ejs templates, in order to view each step of the API access step by step.  Each page represents a new call to the API, leading to the patient everything call at the end.  I took this approach to simplify the basic information needed and to experience the API as well.  To be more secure, better user and token management implemented, but this was not due to time constraints and not using a proper framework to handle everything easier.  

While testing, the Epic (demo) account was not returning any patient data, so I switched to another test account in order to retrieve a working patient $everything call.

This demo account was used - 

Cerner
Test Cerner EHR ID: 4707
wilma_smart / Cerner01
