require('dotenv').config();
const express = require('express');

const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');

const app = express();
const model = require('./model');
const auth = require('./auth');
const { response } = require('express');

app.set('view engine', 'ejs');

const email = process.env.email;
const appid = process.env.appid;

const url = `https://api.1up.health/fhir/dstu2/Patient/`;

app.use(
    cookieSession({
        name: '1upsession',
        keys: ['1key', '2key'], 
        maxAge: 24 * 60 * 60 * 1000
    }),
);
//  Homepage
app.get('/', (req, res) => {
    console.log('add');
    model.addUser(email)
    .then(response => {
        res.cookie('code', response, {maxAge: 24 * 60 * 60 * 1000})
        res.render('pages/index',{email: email, response: response});
    });
});
// Second page - Logging in and shows 
app.get('/login', auth.authUser, (req, res) => {
    console.log('login');
    const a = req.headers.cookie;
    const x = a.split('; ');
    model.getToken(a.substring(5), email)
    .then(response => {
        console.log('getatt');
        console.log(response);
        res.cookie('code', response, {maxAge: 24 * 60 * 60 * 1000});
        res.render('pages/authorize',{email: email, access_token: response, appid: appid});
    });
});
// 
app.get('/callback', auth.authUser, (req, res) => {
    
    res.render('pages/success',{email: email, url: url});
});

app.get('/patient', auth.authUser, (req, res) => {
    console.log('in patient');
    let token = "";
    const a = req.headers.cookie;
    const x = a.split('; ');
    model.getPatient(a.substring(5))
    .then(response => {
        let patientid = response.entry[0].fullUrl;
        const lastItem = patientid.substring(patientid.lastIndexOf('/') + 1);
        console.log(lastItem);
        res.render('pages/patient',{email: email, url: url, patientid: lastItem});
    })
});

app.get('/getPatientEverything/:id', auth.authUser, (req, res) => {
    console.log('in everything');
    id = req.params.id;
    const a = req.headers.cookie;
    const x = a.split('; ');
    let token = "";
    model.getPatientEverything(id, a.substring(5))
    .then(response => {
        console.log(response);
        res.render('pages/patienteverything',{email: email, url: url, data: response});
    })
});

app.listen(3000, err => {
    if (err) throw err;
    console.log('> Ready on http://localhost:3000');
});