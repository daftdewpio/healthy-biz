const { response } = require('express');
const email = process.argv[2];
const model = require('./model');
const auth = require('./auth');


//  Basic setting bearer token using cookie
function authUser(req, res, next) {
    const a = req.headers.cookie;
    const x = a.split('; ');
    console.log(a);
    if (a !== undefined) {
        let token = a.substring(5);
        console.log('in authuser');
        console.log(req.headers);
        next();
    } else {
        res.sendStatus(403);
    }
}

exports.authUser = authUser;
