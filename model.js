require('dotenv').config();
const fetch = require('node-fetch');

const token = {};
const API_URL = `https://api.1up.health`;
const CLIENTID = process.env.clientid;
const CLIENTSECRET = process.env.clientsecret;

// Creates user
async function addUser(email) {

    const url = `${API_URL}/user-management/v1/user?app_user_id=${email}&client_id=${CLIENTID}&client_secret=${CLIENTSECRET}`;

    let response = await fetch(url, {
        method: 'post'
    });
    let json = await response.json();
    if (json.success == true) {
        return json.code;
    } else {
        return getAuthCodeUser(email);
    };
  }

// Gets new auth code from existing user
async function getAuthCodeUser(email) {

    const url = `${API_URL}/user-management/v1/user/auth-code?app_user_id=${email}&client_id=${CLIENTID}&client_secret=${CLIENTSECRET}`;
    let response = await fetch(url, {
        method: 'post'
    });
    let json = await response.json();
    // console.log('getAuth Code');
    // console.log(json.code);
    return json.code;
  }

// Returns access token for user
async function getToken(code, email) {

    const url = `${API_URL}/fhir/oauth2/token?client_id=${CLIENTID}&client_secret=${CLIENTSECRET}&code=${code}&grant_type=authorization_code`;
    let response = await fetch(url, {
        method: 'post'
    });
    let json = await response.json();
    console.log('getToken function');

    token[email] = json.access_token;
    console.log(token);
    return token[email];
}

// Returns patient list
async function getPatient(token) {
    const url = `https://api.1up.health/fhir/dstu2/Patient/`;
    let response = await fetch(url, {
        method: 'get',
        headers: {'authorization' : `Bearer ${token}`}
    });
    let json = await response.json();
    console.log('getPatient function');
    return json;
}

// Returns patient everything call
async function getPatientEverything(patient, token) {
    const url = `https://api.1up.health/fhir/dstu2/Patient/${patient}/$everything`;
    let response = await fetch(url, {
        method: 'get',
        headers: {'authorization' : `Bearer ${token}`}
    });
    let json = await response.json();
    console.log('getPatient everything function');
    console.log(url);
    return json;
}

exports.token = token;
exports.addUser = addUser;
exports.getAuthCodeUser = getAuthCodeUser;
exports.getToken = getToken;
exports.getPatient = getPatient;
exports.getPatientEverything = getPatientEverything;